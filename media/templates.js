define(['handlebars'], function(Handlebars) {

this["JST"] = this["JST"] || {};

this["JST"]["account/alert-msg"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<span class=\"alert-msg check\"><i class=\"fa fa-times-circle\"></i>"
    + escapeExpression(((helper = (helper = helpers.msg || (depth0 != null ? depth0.msg : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"msg","hash":{},"data":data}) : helper)))
    + "</span>\n";
},"useData":true};

this["JST"]["account/base"] = {"1":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "        <div class=\"form-group vendor\">\n            <label class=\"col-lg-2 control-label\">商家:</label>\n            <div class=\"col-lg-7\">\n                <span>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.vendor : depth0)) != null ? stack1.name : stack1), depth0))
    + "</span>\n            </div>\n        </div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, lambda=this.lambda, escapeExpression=this.escapeExpression, functionType="function", helperMissing=helpers.helperMissing, buffer = "<div class=\"col-md-7 personal-info\">\n    <form class=\"form-horizontal\" role=\"form\" action=\"/api/account/\" method=\"put\">\n        <div class=\"form-group username\">\n            <label class=\"col-lg-2 control-label\">用户名:</label>\n            <div class=\"col-lg-7\">\n                <span>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.user : depth0)) != null ? stack1.username : stack1), depth0))
    + "</span>\n            </div>\n        </div>\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.vendor : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "        <div class=\"form-group email\">\n            <label class=\"col-lg-2 control-label\">邮箱:</label>\n            <div class=\"col-lg-7\">\n                <input class=\"form-control\" type=\"text\" name=\"email\" value=\""
    + escapeExpression(((helper = (helper = helpers.email || (depth0 != null ? depth0.email : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"email","hash":{},"data":data}) : helper)))
    + "\">\n            </div>\n        </div>\n        <div class=\"actions\">\n            <!--<input type=\"submit\" class=\"btn-glow primary submit-passwd\" value=\"保存修改\">-->\n            <input type=\"submit\" class=\"btn-theme btn submit-passwd\" value=\"保存修改\">\n        </div>\n    </form>\n</div>\n";
},"useData":true};

this["JST"]["account/password"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "<div class=\"col-md-7 personal-info\">\n    <form class=\"form-horizontal\" role=\"form\" action=\"/api/account/password/\" method=\"put\">\n        <div class=\"form-group username\">\n            <label class=\"col-lg-2 control-label\">用户名:</label>\n            <div class=\"col-lg-7\">\n                <span>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.user : depth0)) != null ? stack1.username : stack1), depth0))
    + "</span>\n            </div>\n        </div>\n        <div class=\"form-group current-passwd\">\n            <label class=\"col-lg-2 control-label\">当前密码:</label>\n            <div class=\"col-lg-7\">\n                <input class=\"form-control\" type=\"password\" name=\"old_password\" value=\"\">\n            </div>\n        </div>\n        <div class=\"form-group new-passwd\">\n            <label class=\"col-lg-2 control-label\">新密码:</label>\n            <div class=\"col-lg-7\">\n                <input class=\"form-control\" type=\"password\" name=\"new_password\" value=\"\">\n            </div>\n        </div>\n        <div class=\"form-group confirm-passwd\">\n            <label class=\"col-lg-2 control-label\">确认新密码:</label>\n            <div class=\"col-lg-7\">\n                <input class=\"form-control\" type=\"password\" name=\"repeat_password\" value=\"\">\n            </div>\n        </div>\n        <div class=\"actions\">\n            <!--<input type=\"submit\" class=\"btn-glow primary submit-passwd\" value=\"保存修改\">-->\n            <input type=\"submit\" class=\"btn-theme btn submit-passwd\" value=\"保存修改\">\n        </div>\n    </form>\n</div>\n";
},"useData":true};

this["JST"]["component/image-edit"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<div class=\"image-resource-editor\">\n  <div class=\"preview-container\">\n    <div class=\"thumbnail-content thumbnail\">\n      <img src=\""
    + escapeExpression(((helper = (helper = helpers.src || (depth0 != null ? depth0.src : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"src","hash":{},"data":data}) : helper)))
    + "\" class=\"thumbnail-img\" />\n    </div>\n  </div>\n  <div class=\"edit-area\">\n    <div class=\"row\">\n      <div class=\"titles col-md-7\">\n        <div class=\"input-block title-block form-group\">\n          <label>标题</label>\n          <input type=\"text\" name=\"title\" placeholder=\"请输入标题\" class=\"inline-input\" value=\""
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "\">\n        </div>\n        <div class=\"input-block subtitle-block form-group\">\n          <label>子标题</label>\n          <input type=\"text\" name=\"subtitle\" placeholder=\"请输入子标题\" class=\"inline-input\" value="
    + escapeExpression(((helper = (helper = helpers.subtiele || (depth0 != null ? depth0.subtiele : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"subtiele","hash":{},"data":data}) : helper)))
    + ">\n        </div>\n      </div>\n      <div class=\"actions col-md-5\">\n        <div class=\"action\">\n          <!--<label class=\"action-title\">旋转</label>-->\n          <a class=\"action-btn action-trigger\" data-action=\"rotate\" data-tooltip=\"旋转\">\n            <i class=\"fa fa-rotate-right\"></i>\n          </a>\n        </div>\n        <div class=\"action\">\n          <a class=\"action-btn action-trigger\" data-action=\"flip\" data-tooltip=\"翻转\">\n            <i class=\"fa fa-exchange\"></i>\n          </a>\n        </div>\n\n      </div>\n\n    </div>\n\n  </div>\n  <div class=\"controls-area\">\n    <div class=\"confirmation text-center\">\n      <a class=\"action-trigger btn btn-primary\" data-action=\"save\" href=\"javascript:void(0)\">保存</a>\n    </div>\n  </div>\n</div>\n\n";
},"useData":true};

this["JST"]["component/modal-dialog"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<div class=\"modal dialog-box\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n        <h4 class=\"modal-title\" id=\""
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "</h4>\n      </div>\n      <div class=\"modal-body\">\n        "
    + escapeExpression(((helper = (helper = helpers.message || (depth0 != null ? depth0.message : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"message","hash":{},"data":data}) : helper)))
    + "\n      </div>\n      <div class=\"modal-footer\">\n        <div class=\"toolbar\"></div>\n      </div>\n    </div>\n  </div>\n</div>\n";
},"useData":true};

this["JST"]["component/resource-popup"] = {"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "width:"
    + escapeExpression(((helper = (helper = helpers.container_width || (depth0 != null ? depth0.container_width : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"container_width","hash":{},"data":data}) : helper)))
    + "px; ";
},"3":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "                    <!--controls preload=\"auto\" width=\""
    + escapeExpression(((helper = (helper = helpers.width || (depth0 != null ? depth0.width : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"width","hash":{},"data":data}) : helper)))
    + "\" height=\""
    + escapeExpression(((helper = (helper = helpers.height || (depth0 != null ? depth0.height : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"height","hash":{},"data":data}) : helper)))
    + "\"-->\n                    <div class=\"video-container\">\n                        <video id=\"theme_video_"
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\"video-js vjs-default-skin vjs-big-play-centered\"\n                            autoplay controls\n                            preload=\"auto\" width=\""
    + escapeExpression(((helper = (helper = helpers.width || (depth0 != null ? depth0.width : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"width","hash":{},"data":data}) : helper)))
    + "\" height=\""
    + escapeExpression(((helper = (helper = helpers.height || (depth0 != null ? depth0.height : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"height","hash":{},"data":data}) : helper)))
    + "\"\n                            poster=\""
    + escapeExpression(((helper = (helper = helpers.thumbnail_img || (depth0 != null ? depth0.thumbnail_img : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"thumbnail_img","hash":{},"data":data}) : helper)))
    + "\">\n                            ";
  stack1 = ((helpers.generateSource || (depth0 && depth0.generateSource) || helperMissing).call(depth0, (depth0 != null ? depth0.url : depth0), {"name":"generateSource","hash":{},"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\n                            <!--<source src=\""
    + escapeExpression(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"url","hash":{},"data":data}) : helper)))
    + "\" type='video/mp4' />-->\n                            <p class=\"vjs-no-js\">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href=\"http://videojs.com/html5-video-support/\" target=\"_blank\">supports HTML5 video</a></p>\n                        </video>\n                    </div>\n";
},"5":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "                    <img src=\""
    + escapeExpression(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"url","hash":{},"data":data}) : helper)))
    + "\" alt=\""
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "\" height=\""
    + escapeExpression(((helper = (helper = helpers.height || (depth0 != null ? depth0.height : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"height","hash":{},"data":data}) : helper)))
    + "\" width=\""
    + escapeExpression(((helper = (helper = helpers.width || (depth0 != null ? depth0.width : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"width","hash":{},"data":data}) : helper)))
    + "\"/>\n";
},"7":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "                    <p class=\"text-content\">"
    + escapeExpression(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"url","hash":{},"data":data}) : helper)))
    + "</p>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<div class=\"modal fade "
    + escapeExpression(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"type","hash":{},"data":data}) : helper)))
    + "-modal resource-modal\" data-type=\""
    + escapeExpression(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"type","hash":{},"data":data}) : helper)))
    + "\" data-url=\""
    + escapeExpression(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"url","hash":{},"data":data}) : helper)))
    + "\">\n    <div class=\"modal-dialog\" style=\"";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.container_width : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += " text-align: center;\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">X</span></button>\n                <h4 class=\"modal-title\">"
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "</h4>\n            </div>\n            <div class=\"modal-body\">\n";
  stack1 = ((helpers.if_eq || (depth0 && depth0.if_eq) || helperMissing).call(depth0, (depth0 != null ? depth0.type : depth0), {"name":"if_eq","hash":{
    'compare': ("video")
  },"fn":this.program(3, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += "\n";
  stack1 = ((helpers.if_eq || (depth0 && depth0.if_eq) || helperMissing).call(depth0, (depth0 != null ? depth0.type : depth0), {"name":"if_eq","hash":{
    'compare': ("image")
  },"fn":this.program(5, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += "\n";
  stack1 = ((helpers.if_eq || (depth0 && depth0.if_eq) || helperMissing).call(depth0, (depth0 != null ? depth0.type : depth0), {"name":"if_eq","hash":{
    'compare': ("text")
  },"fn":this.program(7, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "            </div>\n        </div>\n    </div>\n</div>\n";
},"useData":true};

this["JST"]["component/text-edit"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<div class=\"text-resource-editor\">\n  <div class=\"edit-area\">\n    <div class=\"input-block title-block form-group\">\n      <label>标题</label>\n      <input type=\"text\" autofocus name=\"title\" placeholder=\"请输入标题\" class=\"form-control inline-input\" value=\""
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "\">\n    </div>\n    <div class=\"input-block content-block form-group\">\n      <label>内容</label>\n      <input type=\"text\" name=\"content\" placeholder=\"输入内容\" class=\"form-control inline-input\" value="
    + escapeExpression(((helper = (helper = helpers.content || (depth0 != null ? depth0.content : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"content","hash":{},"data":data}) : helper)))
    + ">\n    </div>\n  </div>\n  <div class=\"controls-area\">\n    <div class=\"confirmation text-center\">\n      <a class=\"action-trigger btn btn-primary\" data-action=\"save\" href=\"javascript:void(0)\">保存</a>\n    </div>\n  </div>\n</div>\n\n";
},"useData":true};

this["JST"]["component/unauthorize"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"unauthorize-container\">\n  <div class=\"content-container\">\n    <div class=\"jumbotron\">\n      <h3>您无权限访问该页面</h3>\n      <div class=\"ctrls\">\n        <a class=\"back-trigger\" href=\"javascript:void(0)\"><i class=\"fa fa-hand-o-left\"></i>点击返回</a>\n      </div>\n    </div>\n  </div>\n</div>\n\n";
  },"useData":true};

this["JST"]["component/video-edit"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<div class=\"video-resource-editor\">\n  <div class=\"preview-container\">\n    <div class=\"thumbnail-content\">\n      <img src=\""
    + escapeExpression(((helper = (helper = helpers.thumbnail_img || (depth0 != null ? depth0.thumbnail_img : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"thumbnail_img","hash":{},"data":data}) : helper)))
    + "\" class=\"thumbnail-img\" />\n    </div>\n  </div>\n  <div class=\"edit-area\">\n      <div class=\"duration\">\n          <h6>起止时间 <div class=\"time-monitor\"><span class=\"start\">"
    + escapeExpression(((helper = (helper = helpers.beginningStr || (depth0 != null ? depth0.beginningStr : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"beginningStr","hash":{},"data":data}) : helper)))
    + "</span> - <span class=\"end\">"
    + escapeExpression(((helper = (helper = helpers.endStr || (depth0 != null ? depth0.endStr : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"endStr","hash":{},"data":data}) : helper)))
    + "</span></div></h6>\n          <div class=\"slider-container flat-slider\">\n              <div class=\"slider\"></div>\n          </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"titles col-md-7\">\n          <div class=\"input-block title-block form-group\">\n            <label>标题</label>\n            <input type=\"text\" name=\"title\" placeholder=\"请输入标题\" class=\"inline-input\" value=\""
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "\">\n          </div>\n          <div class=\"input-block subtitle-block form-group\">\n            <label>子标题</label>\n            <input type=\"text\" name=\"subtiele\" placeholder=\"请输入子标题\" class=\"inline-input\" value="
    + escapeExpression(((helper = (helper = helpers.subtiele || (depth0 != null ? depth0.subtiele : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"subtiele","hash":{},"data":data}) : helper)))
    + ">\n          </div>\n        </div>\n        <div class=\"options col-md-5\">\n          <div class=\"option\">\n            <label class=\"option-title\">播放声音:</label>\n            <span class=\"togglebutton button-xs\">\n              <label>\n                <input type=\"checkbox\" name=\"mute\" checked><span class=\"toggle\"></span>\n              </label>\n            </span>\n          </div>\n          <div class=\"speed option\">\n            <label class=\"option-title\">视频速度:</label>\n              <div class=\"radio-container\">\n                  <label>\n                      <input type=\"radio\" name=\"speed\" value=\"1\" checked/>\n                      正常\n                  </label>\n                  <label>\n                      <input type=\"radio\" name=\"speed\" value=\"0.5\"/>\n                      2倍慢速\n                  </label>\n              </div>\n          </div>\n        </div>\n      </div>\n  </div>\n  <div class=\"controls-area\">\n    <div class=\"confirmation text-center\">\n      <a class=\"action-trigger btn btn-primary\" data-action=\"save\" href=\"javascript:void(0)\">保存</a>\n    </div>\n  </div>\n</div>\n";
},"useData":true};

this["JST"]["edit/edit-header"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<h4 class=\"header-title\">\n  <span>"
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "</span>\n  <a class=\"video-player-trigger\" data-video-url=\""
    + escapeExpression(((helper = (helper = helpers.sample || (depth0 != null ? depth0.sample : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"sample","hash":{},"data":data}) : helper)))
    + "\" href=\"javascript:;\">\n    <i class=\"fa fa-play-circle\"></i>\n  </a>\n</h4>\n<span>";
  stack1 = ((helpers.length_format || (depth0 && depth0.length_format) || helperMissing).call(depth0, (depth0 != null ? depth0._duration : depth0), {"name":"length_format","hash":{},"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</span>\n<span class=\"divider\"></span>\n<a href=\"javascript:void(0)\" class=\"action-trigger\" data-action=\"back-to-detail\" data-toggle=\"tooltip\" title=\"点击返回订单详情\">返回详情</a>\n<a href=\"javascript:void(0)\" class=\"tutorial-trigger\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"点击查看视频编辑教程\">新手教程</a>\n<div class=\"ctrls pull-right\">\n    <span class=\"total-length\">\n      总时长:\n      <span class=\"total\">"
    + escapeExpression(((helper = (helper = helpers.computedLength || (depth0 != null ? depth0.computedLength : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"computedLength","hash":{},"data":data}) : helper)))
    + "</span>\n    </span>\n    <span class=\"last-saved-date\">\n      上次保存时间:\n      <span class=\"date\">"
    + escapeExpression(((helper = (helper = helpers.lastSavedDate || (depth0 != null ? depth0.lastSavedDate : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"lastSavedDate","hash":{},"data":data}) : helper)))
    + "</span>\n    </span>\n    <a class=\"btn btn-complement save-trigger\" data-action=\"save\">保存</a>\n    <a class=\"btn btn-info\" id=\"preview-btn\" data-action=\"preview\">预览视频</a>\n    <a class=\"btn btn-theme\" id=\"render-btn\" data-action=\"render\">渲染视频</a>\n</div>\n";
},"useData":true};

this["JST"]["edit/edit-main"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"edit-header\"></div>\n\n<div class=\"toolbox-container\"></div>\n\n<div class=\"resource-container\">\n  <div class=\"main-space\">\n    <div class=\"music-container music-field\">\n      <div class=\"music-track\"></div>\n    </div>\n    <div class=\"videos-container video-field upload-dropzone\"></div>\n  </div>\n  <div class=\"cache-space\">\n    <div class=\"cache-tray-handle\">\n      <a class=\"handle-toggle\" href=\"javascript:;\"><i class=\"fa fa-chevron-left\"></i></a>\n    </div>\n    <div class=\"resources-container cache-field upload-dropzone\"></div>\n  </div>\n</div>\n\n<div id=\"edit-loading\" class=\"loading-screen\">\n  <div class=\"loading-inner\">\n    <div class=\"loading-center\">\n      <header class=\"loading-header\">正在载入编辑器</header>\n      <div class=\"loading-hinter\">\n        <div class=\"spinner\">\n          <div class=\"dot1 ani-component\"></div>\n          <div class=\"dot2 ani-component\"></div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n";
  },"useData":true};

this["JST"]["edit/edit-music-modal"] = {"1":function(depth0,helpers,partials,data) {
  return "        <button class=\"action-trigger\" data-action=\"cancel\">取消</button>\n      </div>\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<div class=\"music-resource-editor\">\n  <h3>设置背景音乐</h3>\n  <div class=\"edit-area\">\n    <audio src=\""
    + escapeExpression(((helper = (helper = helpers.src || (depth0 != null ? depth0.src : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"src","hash":{},"data":data}) : helper)))
    + "\"></audio>\n    <!--<div class=\"preview-container\">-->\n    <!--</div>-->\n    <div class=\"slider-container flat-slider\">\n      <div class=\"slider\"></div>\n    </div>\n    <div class=\"row\">\n      <span class=\"time-hinter col-md-4\">\n        <label>起止时间:</label>\n        <span class=\"begin-time\">0:00</span>\n        -\n        <span class=\"end-time\">0:00</span>\n      </span>\n      <div class=\"pull-right col-md-2\">\n        <button class=\"btn btn-aqua play\">试听</button>\n        <button class=\"btn btn-slight-danger stop\">停止</button>\n      </div>\n    </div>\n    <div class=\"controls-area\">\n      <div class=\"confirmation text-center\">\n        <button class=\"btn btn-primary action-trigger\" data-action=\"enter\">确定</button>\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0._showCancel : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "    </div>\n  </div>\n</div>\n";
},"useData":true};

this["JST"]["edit/edit-music"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<div class=\"\">\n  <span class=\"title\">\n    "
    + escapeExpression(((helper = (helper = helpers.bgmTitle || (depth0 != null ? depth0.bgmTitle : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"bgmTitle","hash":{},"data":data}) : helper)))
    + "\n  </span>\n  <div class=\"pull-right settings\">\n    <span class=\"time-hinter\">\n      <span class=\"begin-time\">"
    + escapeExpression(((helper = (helper = helpers.bgmBegin || (depth0 != null ? depth0.bgmBegin : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"bgmBegin","hash":{},"data":data}) : helper)))
    + "</span>\n      -\n      <span class=\"end-time\">"
    + escapeExpression(((helper = (helper = helpers.bgmEnd || (depth0 != null ? depth0.bgmEnd : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"bgmEnd","hash":{},"data":data}) : helper)))
    + "</span>\n    </span>\n    <a class=\"js-show-edit a-sleeping\" href=\"javascript:;\">\n      <i class=\"fa fa-cog\"></i>\n    </a>\n  </div>\n</div>\n\n";
},"useData":true};

this["JST"]["edit/edit-toolbox"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"major-tools\">\n  <ul>\n    <!--<li class=\"tool\" data-action=\"change-theme\"><i class=\"fa fa-pencil\"></i>更改模板</li>-->\n    <li class=\"tool\"><a href=\"javascript:;\" data-action=\"upload\"><i class=\"fa fa-upload\"></i>上传视频</a></li>\n    <li class=\"tool\"><a href=\"javascript:;\" data-action=\"add-text\"><i class=\"fa fa-font\"></i>添加文字</a></li>\n    <li class=\"tool\"><a href=\"javascript:;\" data-action=\"undo\"><i class=\"fa fa-undo\"></i>撤销操作</a></li>\n  </ul>\n  <input id=\"fileupload\" type=\"file\" name=\"file\" multiple class=\"hidden\"/>\n</div>\n<div class=\"appendix-tools\">\n  <ul>\n    <li class=\"tool\"><a href=\"javascript:;\" data-action=\"label\"><i class=\"fa fa-star\"></i>标记</a></li>\n    <li class=\"tool\"><a href=\"javascript:;\" data-action=\"duplicate\"><i class=\"fa fa-copy\"></i>复制</a></li>\n    <li class=\"tool\"><a href=\"javascript:;\" data-action=\"delete\"><i class=\"fa fa-trash\"></i>删除</a></li>\n    <li class=\"tool\"><a href=\"javascript:;\" data-action=\"shuffle\"><i class=\"fa fa-close\"></i>打乱顺序</a></li>\n    <li class=\"tool\"><a href=\"javascript:;\" data-action=\"rotate\"><i class=\"fa fa-rotate-right\"></i>旋转(developing)</a></li>\n    <li class=\"tool\"><a href=\"javascript:;\" data-action=\"flip\"><i class=\"fa fa-exchange\"></i>翻转(developing)</a></li>\n  </ul>\n</div>\n";
},"useData":true};

this["JST"]["edit/main-layout"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div id=\"header-container\"></div>\n<div class=\"app-contents\"></div>\n\n";
  },"useData":true};

this["JST"]["edit/resources"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "\n<div class=\"resource-track\">\n    <div class=\"track-wrapper\">\n        <div class=\"track\" id=\"resource\">\n        </div>\n    </div>\n</div>\n\n\n<div class=\"modal fade\" data-type=\"upload\">\n    <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>\n                <h4 class=\"modal-title\">Modal title</h4>\n            </div>\n            <div class=\"modal-body\">\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n                <button type=\"button\" class=\"btn btn-primary\">Save changes</button>\n            </div>\n        </div><!-- /.modal-content -->\n    </div><!-- /.modal-dialog -->\n</div><!-- /.modal -->\n";
},"useData":true};

this["JST"]["edit/video-item"] = {"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<div class=\"video-clip uploading\">\n    <div class=\"clip-head sortable-handle\" data-name=\""
    + escapeExpression(((helper = (helper = helpers._filename || (depth0 != null ? depth0._filename : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"_filename","hash":{},"data":data}) : helper)))
    + "\" >\n        <h6>\n            <div class=\"name\" title="
    + escapeExpression(((helper = (helper = helpers._filename || (depth0 != null ? depth0._filename : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"_filename","hash":{},"data":data}) : helper)))
    + ">"
    + escapeExpression(((helper = (helper = helpers._filename || (depth0 != null ? depth0._filename : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"_filename","hash":{},"data":data}) : helper)))
    + "</div>\n        </h6>\n    </div>\n    <div class=\"content-container\">\n        <div class=\"clip-cover\">\n            <span>正在上传 "
    + escapeExpression(((helper = (helper = helpers._progress || (depth0 != null ? depth0._progress : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"_progress","hash":{},"data":data}) : helper)))
    + " %</span>\n            <div class=\"progress\">\n                <div class=\"progress-bar progress-bar-striped active\" role=\"progressbar\" aria-valuenow=\""
    + escapeExpression(((helper = (helper = helpers._progress || (depth0 != null ? depth0._progress : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"_progress","hash":{},"data":data}) : helper)))
    + "\"\n                     aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: "
    + escapeExpression(((helper = (helper = helpers._progress || (depth0 != null ? depth0._progress : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"_progress","hash":{},"data":data}) : helper)))
    + "%;\">\n                    <span class=\"text\"></span>\n                    <span class=\"sr-only\">"
    + escapeExpression(((helper = (helper = helpers._progress || (depth0 != null ? depth0._progress : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"_progress","hash":{},"data":data}) : helper)))
    + " %</span>\n                </div>\n            </div>\n        </div>\n        <div class=\"actions text-center\">\n          <a class=\"btn btn-danger btn-xs action-trigger\" href=\"javascript:void(0)\" data-action=\"cancel-upload\"><i class=\"fa fa-close\"></i></a>\n        </div>\n    </div>\n</div>\n";
},"3":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<div class=\"video-clip ";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.shouldHide : depth0), {"name":"if","hash":{},"fn":this.program(4, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "\" data-id=\""
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-fid="
    + escapeExpression(((helper = (helper = helpers._fid || (depth0 != null ? depth0._fid : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"_fid","hash":{},"data":data}) : helper)))
    + ">\n    <div class=\"clip-head sortable-handle\">\n        <h6>\n            <div class=\"name\" title="
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + ">"
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "</div>\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.headerActionEnabled : depth0), {"name":"if","hash":{},"fn":this.program(6, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "        </h6>\n    </div>\n\n    <div class=\"content-container sequence-container sortable-handle\">\n        <div class=\"clip-cover sequence-trigger\">\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.showPlayAction : depth0), {"name":"if","hash":{},"fn":this.program(17, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "            <div class=\"thumbnail-content\">\n";
  stack1 = ((helpers.if_eq || (depth0 && depth0.if_eq) || helperMissing).call(depth0, (depth0 != null ? depth0.thumbnailType : depth0), {"name":"if_eq","hash":{
    'compare': ("image")
  },"fn":this.program(22, data),"inverse":this.program(24, data),"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += "            </div>\n        </div>\n\n        <div class=\"clip-info\">\n";
  stack1 = ((helpers.if_eq || (depth0 && depth0.if_eq) || helperMissing).call(depth0, (depth0 != null ? depth0.resource_type : depth0), {"name":"if_eq","hash":{
    'compare': ("image")
  },"fn":this.program(27, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  stack1 = ((helpers.if_eq || (depth0 && depth0.if_eq) || helperMissing).call(depth0, (depth0 != null ? depth0.resource_type : depth0), {"name":"if_eq","hash":{
    'compare': ("video")
  },"fn":this.program(29, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  stack1 = ((helpers.if_eq || (depth0 && depth0.if_eq) || helperMissing).call(depth0, (depth0 != null ? depth0.resource_type : depth0), {"name":"if_eq","hash":{
    'compare': ("audio")
  },"fn":this.program(31, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  stack1 = ((helpers.if_eq || (depth0 && depth0.if_eq) || helperMissing).call(depth0, (depth0 != null ? depth0.resource_type : depth0), {"name":"if_eq","hash":{
    'compare': ("text")
  },"fn":this.program(33, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0._durationText : depth0), {"name":"if","hash":{},"fn":this.program(35, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "        </div>\n\n    </div>\n</div>\n\n";
},"4":function(depth0,helpers,partials,data) {
  return "resource-used";
  },"6":function(depth0,helpers,partials,data) {
  var stack1, buffer = "                <div class=\"actions pull-right\">\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.labelActionEnabled : depth0), {"name":"if","hash":{},"fn":this.program(7, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.editEnabled : depth0), {"name":"if","hash":{},"fn":this.program(12, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "                </div>\n";
},"7":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.labeled : depth0), {"name":"if","hash":{},"fn":this.program(8, data),"inverse":this.program(10, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"8":function(depth0,helpers,partials,data) {
  return "                            <a class=\"clip-star action-trigger labeled-star\" data-action=\"one-unlabel\"\n                               href=\"javascript:void(0)\"><i class=\"fa fa-star\"></i></a>\n";
  },"10":function(depth0,helpers,partials,data) {
  return "                            <a class=\"clip-star action-trigger\" href=\"javascript:void(0)\"  data-action=\"one-label\"\n                                    ><i class=\"fa fa-star\"></i></a>\n";
  },"12":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.isEditing : depth0), {"name":"if","hash":{},"fn":this.program(13, data),"inverse":this.program(15, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"13":function(depth0,helpers,partials,data) {
  return "                            <a class=\"clip-cog action-trigger\" data-action=\"end-edit\" href=\"javascript:void(0)\"><i class=\"fa fa-chevron-left\"></i></a>\n";
  },"15":function(depth0,helpers,partials,data) {
  return "                            <a class=\"clip-cog action-trigger\" data-action=\"edit\" href=\"javascript:void(0)\"><i class=\"fa fa-cog\"></i></a>\n";
  },"17":function(depth0,helpers,partials,data) {
  var stack1, buffer = "                <a class=\"video-play-trigger js-selectable-cancel\" href=\"javascript:void(0)\">\n                  <!--<i class=\"fa ";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.playIconClass : depth0), {"name":"if","hash":{},"fn":this.program(18, data),"inverse":this.program(20, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\"></i>-->\n                  <i class=\"fa\"></i>\n                </a>\n";
},"18":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return escapeExpression(((helper = (helper = helpers.playIconClass || (depth0 != null ? depth0.playIconClass : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"playIconClass","hash":{},"data":data}) : helper)));
  },"20":function(depth0,helpers,partials,data) {
  return "fa-play-circle";
  },"22":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "                <img src=\""
    + escapeExpression(((helper = (helper = helpers.img || (depth0 != null ? depth0.img : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"img","hash":{},"data":data}) : helper)))
    + "\">\n";
},"24":function(depth0,helpers,partials,data) {
  var stack1, helperMissing=helpers.helperMissing, buffer = "";
  stack1 = ((helpers.if_eq || (depth0 && depth0.if_eq) || helperMissing).call(depth0, (depth0 != null ? depth0.thumbnailType : depth0), {"name":"if_eq","hash":{
    'compare': ("text")
  },"fn":this.program(25, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"25":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "                <div class=\"thumbnail-text\" title="
    + escapeExpression(((helper = (helper = helpers.content || (depth0 != null ? depth0.content : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"content","hash":{},"data":data}) : helper)))
    + ">"
    + escapeExpression(((helper = (helper = helpers.content || (depth0 != null ? depth0.content : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"content","hash":{},"data":data}) : helper)))
    + "</div>\n";
},"27":function(depth0,helpers,partials,data) {
  return "                <i class=\"fa fa-image\"></i>\n";
  },"29":function(depth0,helpers,partials,data) {
  return "                <i class=\"fa fa-film\"></i>\n";
  },"31":function(depth0,helpers,partials,data) {
  return "                <i class=\"fa fa-volume-up\"></i>\n";
  },"33":function(depth0,helpers,partials,data) {
  return "                <i class=\"fa bm-text\"></i>\n";
  },"35":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "              <span class=\"hovershow-container pull-right duration-text\">\n                "
    + escapeExpression(((helper = (helper = helpers._durationText || (depth0 != null ? depth0._durationText : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"_durationText","hash":{},"data":data}) : helper)))
    + "\n              </span>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0._uploading : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(3, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\n";
},"useData":true};

this["JST"]["edit/videos"] = {"1":function(depth0,helpers,partials,data) {
  return "<div class=\"theme-track\">\n    <h5>主题模板</h5>\n    <div class=\"track-wrapper\">\n        <div class=\"track theme\">\n        </div>\n    </div>\n</div>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0._showTheme : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\n<div class=\"video-track\">\n    <!--<h5>视频编辑</h5>-->\n    <div class=\"track-wrapper\">\n        <div class=\"track video\">\n        </div>\n    </div>\n    <div class=\"errors-container\" style=\"display:none;\">\n      <div class=\"title\">\n        <span>错误提示</span>\n        <div class=\"pull-right\">\n          <a href=\"javascript:void(0)\" class=\"btn btn-xs btn-close\"><i class=\"fa fa-close\"></i></a>\n        </div>\n      </div>\n      <ul class=\"errors\"></ul>\n    </div>\n</div>\n";
},"useData":true};

this["JST"]["home/home-main"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"home-wrapper\">\n  <div class=\"jumbotron text-center\">\n    <h1>主页</h1>\n  </div>\n</div>\n";
  },"useData":true};

this["JST"]["layout/footer"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<!-- starts footer -->\n<footer id=\"footer\">\n    <div class=\"container\">\n        <div class=\"row credits\">\n            <div class=\"col-md-12\">\n                <div class=\"row copyright\">\n                    <div class=\"col-md-12\">\n                        <a href=\"#\">返回首页</a>\n                        <span>|</span>\n                        <span>© Copyright BestMinr Inc.</span>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</footer>";
  },"useData":true};

this["JST"]["layout/header"] = {"1":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "        <li class=\"notification-dropdown hidden-xs hidden-sm shopcart\">\n            <a href=\"javascript:void(0)\" class=\"trigger\" data-trigger=\"mouseenter\">\n                <i class=\"fa fa-shopping-cart\"></i>\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.shopcart_count : depth0), {"name":"if","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "            </a>\n            <div class=\"pop-dialog\">\n                <div class=\"pointer right\">\n                    <div class=\"arrow\"></div>\n                    <div class=\"arrow_border\"></div>\n                </div>\n                <div class=\"body\">\n                    <a href=\"#\" class=\"close-icon\"><i class=\"icon-remove-sign\"></i></a>\n                    <div class=\"messages\">\n                        <h3>共选择了 <span class=\"count-hinter\">"
    + escapeExpression(((helper = (helper = helpers.shopcart_count || (depth0 != null ? depth0.shopcart_count : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"shopcart_count","hash":{},"data":data}) : helper)))
    + " </span>个主题</h3>\n";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.theme_in_shopcart : depth0), {"name":"each","hash":{},"fn":this.program(4, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "                        <div class=\"footer\">\n                            <a href=\"javascript:void(0)\" class=\"logout action-trigger\" data-action=\"create-order\">创建订单</a>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </li>\n";
},"2":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "                <span class=\"count count-hinter\">"
    + escapeExpression(((helper = (helper = helpers.shopcart_count || (depth0 != null ? depth0.shopcart_count : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"shopcart_count","hash":{},"data":data}) : helper)))
    + "</span>\n";
},"4":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, lambda=this.lambda;
  return "                            <div class=\"item\" data-id=\""
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n                                <a href=\"javascript:void(0)\"><img src=\""
    + escapeExpression(((helper = (helper = helpers.thumbnail_img || (depth0 != null ? depth0.thumbnail_img : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"thumbnail_img","hash":{},"data":data}) : helper)))
    + "\" class=\"display\" /></a>\n                                <a href=\"javascript:void(0)\" class=\"name\">"
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "</a>\n                                <div class=\"msg\">\n                                    "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.category : depth0)) != null ? stack1.name : stack1), depth0))
    + "\n                                </div>\n                                <a href=\"javascript:void(0)\" class=\"time action-trigger\" data-action=\"delete-shopcart-theme\" data-id=\""
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\"><i class=\"fa fa-trash\"></i> 删除</a>\n                            </div>\n";
},"6":function(depth0,helpers,partials,data) {
  return "                <li><a href=\"/order/\">我的订单</a></li>\n";
  },"8":function(depth0,helpers,partials,data) {
  return "                <li><a href=\"/videos/\">我的视频</a></li>\n";
  },"10":function(depth0,helpers,partials,data) {
  return "                <li><a href=\"/manage/\">业务管理</a></li>\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<!-- navbar -->\n<div class=\"container\">\n    <div class=\"navbar-header\">\n        <button class=\"navbar-toggle\" type=\"button\" data-toggle=\"collapse\" id=\"menu-toggler\">\n            <span class=\"sr-only\">Toggle navigation</span>\n            <span class=\"icon-bar\"></span>\n            <span class=\"icon-bar\"></span>\n            <span class=\"icon-bar\"></span>\n        </button>\n        <a class=\"navbar-brand\" href=\"/home/\">云影</a>\n    </div>\n    <ul class=\"nav navbar-nav pull-right hidden-xs\">\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.isVendor : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "\n        <li class=\"dropdown\">\n            <a href=\"#\" class=\"dropdown-toggle hidden-xs hidden-sm\" data-toggle=\"dropdown\" user-id=\""
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n                "
    + escapeExpression(((helper = (helper = helpers.username || (depth0 != null ? depth0.username : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"username","hash":{},"data":data}) : helper)))
    + "\n                <b class=\"caret\"></b>\n            </a>\n            <ul class=\"dropdown-menu\">\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.isVendor : depth0), {"name":"if","hash":{},"fn":this.program(6, data),"inverse":this.program(8, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "                <li><a href=\"/theme/\">主题模板</a></li>\n                <li><a href=\"/account/\">账号设置</a></li>\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.isAdmin : depth0), {"name":"if","hash":{},"fn":this.program(10, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "                <li><a id=\"signout\" href=\"javascript:;\">退出登录</a></li>\n            </ul>\n        </li>\n        <!--<li class=\"settings hidden-xs hidden-sm\">-->\n            <!--<a href=\"#\" role=\"button\" id=\"signout\">-->\n                <!--<i class=\"fa fa-sign-out\"></i>-->\n            <!--</a>-->\n        <!--</li>-->\n    </ul>\n    <div class=\"global-tip\">\n        <div class=\"tip alert\">\n            <!-- alert content-->\n        </div>\n    </div>\n</div>\n\n<!--</header>-->\n<!-- end navbar -->\n";
},"useData":true};

this["JST"]["layout/paginator"] = {"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "  <li><a href=\"#\" class=\"previous\" data-page="
    + escapeExpression(((helper = (helper = helpers.prev || (depth0 != null ? depth0.prev : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"prev","hash":{},"data":data}) : helper)))
    + "><i class=\"fa fa-angle-left\"></i></a></li>\n";
},"3":function(depth0,helpers,partials,data) {
  return "  <li class=\"disabled\"><a href=\"#\" class=\"previous\" ><i class=\"fa fa-angle-left\"></i></a></li>\n";
  },"5":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "    <li><input type=\"text\" data-max-page=\""
    + escapeExpression(((helper = (helper = helpers.last || (depth0 != null ? depth0.last : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"last","hash":{},"data":data}) : helper)))
    + "\" value=\""
    + escapeExpression(((helper = (helper = helpers.page || (depth0 != null ? depth0.page : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"page","hash":{},"data":data}) : helper)))
    + "/"
    + escapeExpression(((helper = (helper = helpers.last || (depth0 != null ? depth0.last : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"last","hash":{},"data":data}) : helper)))
    + "\"/></li>\n";
},"7":function(depth0,helpers,partials,data,depths) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.pages : depth0), {"name":"each","hash":{},"fn":this.program(8, data, depths),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"8":function(depth0,helpers,partials,data,depths) {
  var stack1, helperMissing=helpers.helperMissing, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "      <li ";
  stack1 = ((helpers.if_eq || (depth0 && depth0.if_eq) || helperMissing).call(depth0, depth0, {"name":"if_eq","hash":{
    'compare': ((depths[1] != null ? depths[1].page : depths[1]))
  },"fn":this.program(9, data, depths),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "><a href=\"#\" data-page=\""
    + escapeExpression(lambda(depth0, depth0))
    + "\">"
    + escapeExpression(lambda(depth0, depth0))
    + "</a></li>\n";
},"9":function(depth0,helpers,partials,data) {
  return "class=\"active\"";
  },"11":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "  <li><a href=\"#\" class=\"next\" data-page=\""
    + escapeExpression(((helper = (helper = helpers.next || (depth0 != null ? depth0.next : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"next","hash":{},"data":data}) : helper)))
    + "\"><i class=\"fa fa-angle-right\"></i></a></li>\n";
},"13":function(depth0,helpers,partials,data) {
  return "  <li class=\"disabled\"><a href=\"#\" class=\"next\"><i class=\"fa fa-angle-right\"></i></a></li>\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data,depths) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<ul class=\"pagination\">\n  <li><a href=\"#\" class=\"first\" data-action=\"first\" data-page=\""
    + escapeExpression(((helper = (helper = helpers.first || (depth0 != null ? depth0.first : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"first","hash":{},"data":data}) : helper)))
    + "\"><i class=\"fa fa-angle-double-left\"></i></a></li>\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.prev : depth0), {"name":"if","hash":{},"fn":this.program(1, data, depths),"inverse":this.program(3, data, depths),"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "\n";
  stack1 = ((helpers.if_eq || (depth0 && depth0.if_eq) || helperMissing).call(depth0, (depth0 != null ? depth0.pagerType : depth0), {"name":"if_eq","hash":{
    'compare': ("simple")
  },"fn":this.program(5, data, depths),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  stack1 = ((helpers.if_eq || (depth0 && depth0.if_eq) || helperMissing).call(depth0, (depth0 != null ? depth0.pagerType : depth0), {"name":"if_eq","hash":{
    'compare': ("pages")
  },"fn":this.program(7, data, depths),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += "\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.next : depth0), {"name":"if","hash":{},"fn":this.program(11, data, depths),"inverse":this.program(13, data, depths),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\n  <li><a href=\"#\" class=\"last\" data-action=\"last\" data-page=\""
    + escapeExpression(((helper = (helper = helpers.last || (depth0 != null ? depth0.last : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"last","hash":{},"data":data}) : helper)))
    + "\"><i class=\"fa fa-angle-double-right\"></i></a></li>\n</ul>\n";
},"useData":true,"useDepths":true};

this["JST"]["manage/create_user_dialog"] = {"1":function(depth0,helpers,partials,data) {
  return "修改用户";
  },"3":function(depth0,helpers,partials,data) {
  return "添加用户";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, lambda=this.lambda, escapeExpression=this.escapeExpression, functionType="function", helperMissing=helpers.helperMissing, buffer = "    <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\" id=\"close\">×</span><span class=\"sr-only\">关闭</span></button>\n                <h4 class=\"modal-title\" id=\"myLargeModalLabel\">添加用户</h4>\n            </div>\n            <div class=\"modal-body row\">\n                <div class=\"col-md-12 personal-info in-modal\">\n                    <form class=\"form-horizontal\" role=\"form\">\n                        <div class=\"form-group vendor\">\n                            <label class=\"col-lg-2 control-label\">商家:</label>\n                            <div class=\"col-lg-7\">\n                                <span>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.vendor : depth0)) != null ? stack1.name : stack1), depth0))
    + "</span>\n                                <!--<input class=\"form-control\" type=\"text\" value=\"admin\" disabled=\"true\">-->\n                            </div>\n                        </div>\n                        <div class=\"form-group username\">\n                            <label class=\"col-lg-2 control-label\">用户名:</label>\n                            <div class=\"col-lg-6\">\n                                <input class=\"form-control\" type=\"text\" name=\"username\" value=\""
    + escapeExpression(((helper = (helper = helpers.username || (depth0 != null ? depth0.username : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"username","hash":{},"data":data}) : helper)))
    + "\">\n                            </div>\n                        </div>\n                        <div class=\"form-group email\">\n                            <label class=\"col-lg-2 control-label\">邮箱:</label>\n                            <div class=\"col-lg-6\">\n                                <input class=\"form-control\" type=\"text\" name=\"email\" value=\""
    + escapeExpression(((helper = (helper = helpers.email || (depth0 != null ? depth0.email : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"email","hash":{},"data":data}) : helper)))
    + "\">\n                            </div>\n                            <!-- <span class=\"alert-msg check\"><i class=\"fa fa-times-circle\"></i> 请输入正确邮箱</span> -->\n                        </div>\n                        <div class=\"form-group new-passwd\">\n                            <label class=\"col-lg-2 control-label\">密码:</label>\n                            <div class=\"col-lg-6\">\n                                <input class=\"form-control\" type=\"password\" name=\"password\" value=\"\">\n                            </div>\n                            <!-- <span class=\"alert-msg check\"><i class=\"fa fa-times-circle\"></i> 请输入密码</span> -->\n                        </div>\n                        <div class=\"form-group confirm-passwd\">\n                            <label class=\"col-lg-2 control-label\">确认密码:</label>\n                            <div class=\"col-lg-6\">\n                                <input class=\"form-control\" type=\"password\" name=\"repeat_password\" value=\"\">\n                            </div>\n                            <!-- <span class=\"alert-msg check\"><i class=\"fa fa-times-circle\"></i> 请再次输入密码</span> -->\n                        </div>\n                        <div class=\"form-group set-admin\">\n                            <label class=\"col-lg-2 control-label\">设置权限:</label>\n                            <div class=\"col-lg-6\">\n                                <select name=\"permission\" value=\""
    + escapeExpression(((helper = (helper = helpers.permission || (depth0 != null ? depth0.permission : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"permission","hash":{},"data":data}) : helper)))
    + "\" class=\"selectpicker\">\n                                    <option value=\"normal\">普通成员</option>\n                                    <option value=\"admin\">管理员</option>\n                                </select>\n                            </div>\n                        </div>\n                    </form>\n                </div>\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">取消</button>\n                <button type=\"button\" class=\"btn btn-theme create-user\">";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.isEditing : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(3, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</button>\n            </div>\n        </div>\n    </div>\n";
},"useData":true};

this["JST"]["manage/user"] = {"1":function(depth0,helpers,partials,data) {
  return "        <li class=\"last\"><i class=\"fa fa-remove delete-user\"></i></li>\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<td class=\"username\">\n    <!--<input type=\"checkbox\">-->\n    <a href=\"#\">"
    + escapeExpression(((helper = (helper = helpers.username || (depth0 != null ? depth0.username : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"username","hash":{},"data":data}) : helper)))
    + "</a>\n</td>\n<td class=\"\">\n    "
    + escapeExpression(((helper = (helper = helpers.email || (depth0 != null ? depth0.email : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"email","hash":{},"data":data}) : helper)))
    + "\n</td>\n<td class=\"\">\n    "
    + escapeExpression(((helper = (helper = helpers.orders_count || (depth0 != null ? depth0.orders_count : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"orders_count","hash":{},"data":data}) : helper)))
    + "\n</td>\n<td class=\"\">\n    "
    + escapeExpression(((helper = (helper = helpers.permission_name || (depth0 != null ? depth0.permission_name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"permission_name","hash":{},"data":data}) : helper)))
    + "\n</td>\n<td>\n    <ul class=\"actions\">\n        <li><i class=\"fa fa-cog update-user\" data-toggle=\"modal\" data-target=\"#createModal\"></i></li>\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.isMe : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "    </ul>\n</td>\n";
},"useData":true};

this["JST"]["manage/user_option"] = {"1":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "  <option value=\""
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = helpers.username || (depth0 != null ? depth0.username : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"username","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "";
  stack1 = helpers.each.call(depth0, (depth0 != null ? depth0.users : depth0), {"name":"each","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"useData":true};

this["JST"]["orders/create_order"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div id=\"pad-wrapper\" class=\"form-page\">\n  <div class=\"page__header\">\n    <div class=\"container\">\n      <h3 class=\"title\">创建订单</h3>\n    </div>\n  </div>\n  <div class=\"container\">\n    <div class=\"form-wrapper\">\n      <form>\n        <div class=\"field-box order-name\">\n          <label class=\"field-label\">订单名称:</label>\n          <div class=\"col-md-8\">\n            <input class=\"form-control\" name=\"title\" data-toggle=\"tooltip\" data-trigger=\"focus\" title=\"\" data-placement=\"top\" type=\"text\" data-original-title=\"输入名称\">\n          </div>\n        </div>\n        <div class=\"field-box order-desc\">\n          <label class=\"field-label\">订单描述:</label>\n          <div class=\"col-md-8\">\n            <textarea class=\"form-control field\" name=\"description\" rows=\"4\" placeholder=\"输入描述信息\"></textarea>\n          </div>\n        </div>\n        <div class=\"field-box choose-theme\">\n          <label class=\"field-label\">已选择主题:</label>\n          <div class=\"col-md-8\">\n            <div class=\"panel panel-default table-products\">\n              <table class=\"table table-hover\">\n                <thead>\n                  <tr>\n                    <th class=\"col-md-3\">\n                      主题\n                    </th>\n                    <th class=\"col-md-2\">\n                      <span class=\"line\"></span>名称\n                    </th>\n                    <th class=\"col-md-2\">\n                      <span class=\"line\"></span>类型\n                    </th>\n                    <th class=\"col-md-2\">\n                      <span class=\"line\"></span>操作\n                    </th>\n                  </tr>\n                </thead>\n                <tbody class=\"theme-list-container\">\n                </tbody>\n              </table>\n            </div>\n          </div>\n        </div>\n        <div class=\"field-box theme-input\">\n          <label class=\"field-label\">添加主题:</label>\n          <div class=\"col-md-8\">\n            <div class=\"input-group\">\n              <input type=\"text\" class=\"form-control\" placeholder=\"输入主题编号\">\n              <span class=\"input-group-btn\">\n                <button class=\"btn btn-success add-theme\" type=\"button\"><i class=\"icon-plus\"></i> 添加</button>\n              </span>\n            </div>\n          </div>\n        </div>\n      </form>\n      <div class=\"submit-action\">\n        <div class=\"col-md-8 col-md-offset-1\">\n          <a class=\"btn btn-primary save\">完成<i class=\"fa fa-rocket\"></i></a>\n          <a class=\"btn btn-subtle cancel\">取消</a>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n";
  },"useData":true};

this["JST"]["orders/detail/head"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<div class=\"page__header\">\n  <div class=\"container\">\n    <h3>\n      <div class=\"breadcrumb\">\n        <li><a href=\"/order/\">我的订单</a></li>\n        <li class=\"active\">"
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "</li>\n      </div>\n    </h3>\n  </div>\n</div>\n<div class=\"container order-desc\">\n  <h4>"
    + escapeExpression(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"description","hash":{},"data":data}) : helper)))
    + "</h4>\n</div>\n\n";
},"useData":true};

this["JST"]["orders/order"] = {"1":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "    <td>\n      "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.user : depth0)) != null ? stack1.username : stack1), depth0))
    + "\n    </td>\n";
},"3":function(depth0,helpers,partials,data) {
  return "            <span class=\"label label-aqua\">渲染中</span>\n";
  },"5":function(depth0,helpers,partials,data) {
  return "            <span class=\"label label-success\">已完成</span>\n";
  },"7":function(depth0,helpers,partials,data) {
  return "            <span class=\"label label-warning\">待编辑</span>\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<!-- single order row -->\n    <td>\n        <!--<input type=\"checkbox\">-->\n        <a href=\""
    + escapeExpression(((helper = (helper = helpers.link || (depth0 != null ? depth0.link : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"link","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "</a>\n    </td>\n    <td class=\"\">\n        "
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\n    </td>\n    <td class=\"\">\n        "
    + escapeExpression(((helper = (helper = helpers.created_at || (depth0 != null ? depth0.created_at : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"created_at","hash":{},"data":data}) : helper)))
    + "\n    </td>\n    <td class=\"\">\n        "
    + escapeExpression(((helper = (helper = helpers.videos_count || (depth0 != null ? depth0.videos_count : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"videos_count","hash":{},"data":data}) : helper)))
    + "\n    </td>\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.isFromVendor : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "    <td>\n";
  stack1 = ((helpers.if_eq || (depth0 && depth0.if_eq) || helperMissing).call(depth0, (depth0 != null ? depth0.status : depth0), {"name":"if_eq","hash":{
    'compare': ("rendering")
  },"fn":this.program(3, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  stack1 = ((helpers.if_eq || (depth0 && depth0.if_eq) || helperMissing).call(depth0, (depth0 != null ? depth0.status : depth0), {"name":"if_eq","hash":{
    'compare': ("finished")
  },"fn":this.program(5, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  stack1 = ((helpers.if_eq || (depth0 && depth0.if_eq) || helperMissing).call(depth0, (depth0 != null ? depth0.status : depth0), {"name":"if_eq","hash":{
    'compare': ("editing")
  },"fn":this.program(7, data),"inverse":this.noop,"data":data}));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "    </td>\n    <td>\n        <ul class=\"actions\">\n            <!--<li><i class=\"fa fa-search\"></i></li>-->\n            <!--<li><i class=\"fa fa-cog\"></i></li>-->\n            <li class=\"last\">\n              <a class=\"btn btn-round btn-round-mini btn-slight-danger btn-sleeping\" href=\"javascript:;\">\n                <i class=\"fa fa-remove delete-order action-trigger\" data-action=\"delete\"></i>\n              </a>\n            </li>\n        </ul>\n    </td>\n";
},"useData":true};

this["JST"]["orders/order_detail"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<div class=\"row header order-name\">\n    <div class=\"col-md-12 col-md-offset-1\">\n        <div class=\"breadcrumb\">\n            <li><a href=\"/order/\">我的订单</a></li>\n            <li class=\"active\">"
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "</li>\n        </div>\n        <h3>"
    + escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"title","hash":{},"data":data}) : helper)))
    + "</h3>\n    </div>\n</div>\n<div class=\"row header order-desc\">\n    <div class=\"col-md-12 col-md-offset-1\">\n        <h4>"
    + escapeExpression(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"description","hash":{},"data":data}) : helper)))
    + "</h4>\n    </div>\n</div>";
},"useData":true};

this["JST"]["orders/order_detail_main"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div id=\"pad-wrapper\">\n  <div class=\"detail-header-container\">\n  </div>\n  <div class=\"order-video container\">\n      <div class=\"\">\n          <!--<ul class=\"nav nav-tabs\" role=\"tablist\">-->\n              <!--<li class=\"active\"><a href=\"#video\">视频</a></li>-->\n          <!--</ul>-->\n          <div id=\"video-content\" class=\"tab-content\">\n              <div id=\"video-list\" class=\"video-gallery\"></div>\n              <!--<div class=\"tab-pane row fade in active\" id=\"video-list\">-->\n              <!--</div>-->\n          </div>\n      </div>\n  </div>\n</div>\n\n";
  },"useData":true};

this["JST"]["orders/order_main"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div id=\"pad-wrapper\">\n  <div class=\"page__header\">\n    <div class=\"container\">\n      <h3 class=\"title\">我的订单</h3>\n    </div>\n  </div>\n  <div class=\"container\">\n    <div class=\"queries-container clearfix\">\n      <!--for query filters-->\n    </div>\n    <div class=\"order-list table-products section panel panel-default\">\n      <table class=\"table table-hover\">\n        <thead>\n          <tr>\n            <th class=\"col-md-3\">\n            </span>订单名称\n            </th>\n            <th class=\"col-md-2\">\n              <span class=\"line\"></span>订单号\n            </th>\n            <th class=\"col-md-2\">\n              <span class=\"line\"></span>创建时间\n            </th>\n            <th class=\"col-md-1\">\n              <span class=\"line\"></span>视频数量\n            </th>\n            <th class=\"col-md-2\">\n              <span class=\"line\"></span>状态\n            </th>\n            <th class=\"col-md-2\">\n              <span class=\"line\"></span>管理\n            </th>\n          </tr>\n        </thead>\n        <tbody class=\"list-items-container\">\n        </tbody>\n      </table>\n      <div class=\"pagination-container text-center\">\n        <!--for paginator -->\n      </div>\n    </div>\n  </div>\n\n</div>\n\n";
  },"useData":true};

this["JST"]["orders/query_selector"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"order-status col-md-7 row\">\n  <div class=\"queries__list-filter pull-left\">\n    <ul class=\"category\">\n      <li order-status-value=\"all\" class=\"selected\">全部</li>\n      <li order-status-value=\"editing\" class=\"\">待编辑</li>\n      <li order-status-value=\"finished\" class=\"\">已完成</li>\n      <li order-status-value=\"rendering\" class=\"\">渲染中</li>\n    </ul>\n  </div>\n  <div class=\"col-md-4 queries__dropdown-filter\">\n    <select class=\"selectpicker js-date-range\">\n      <option value=\"\">按时间过滤</option>\n      <option value=\"0\">今天</option>\n      <option value=\"7\">最近7天</option>\n      <option value=\"30\">最近30天</option>\n      <option value=\"90\">最近3个月</option>\n    </select>\n  </div>\n</div>\n\n<div class=\"filter-block pull-right col-md-5 row\">\n  <div class=\"queries__filter-section\">\n    <a class=\"btn btn-complement action-trigger\" data-action=\"create-order\">+ 创建订单</a>\n  </div>\n  <div class=\"queries__filter-section\">\n    <div class=\"input-addon-container\">\n      <input type=\"text\" class=\"search search-order form-control\" placeholder=\"搜索订单\">\n      <span class=\"addon-icon show-when-focus\">\n        <a class=\"search-trigger\" href=\"javascript:void(0)\"> <i class=\"fa fa-search\"></i> </a>\n      </span>\n    </div>\n  </div>\n</div>\n\n";
  },"useData":true};

this["JST"]["orders/video"] = {"1":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "            <img src=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.render : depth0)) != null ? stack1.thumbnail_img : stack1), depth0))
    + "\">\n";
},"3":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "            <div class=\"no-preview\">\n                <h3>"
    + escapeExpression(((helper = (helper = helpers.statusText || (depth0 != null ? depth0.statusText : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"statusText","hash":{},"data":data}) : helper)))
    + "</h3>\n            </div>\n";
},"5":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, lambda=this.lambda;
  return "        <a class=\"task-title action-trigger ready-for-share\" data-action=\"share\" href=\""
    + escapeExpression(((helper = (helper = helpers.share_url || (depth0 != null ? depth0.share_url : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"share_url","hash":{},"data":data}) : helper)))
    + "\" target=\"_blank\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.theme : depth0)) != null ? stack1.name : stack1), depth0))
    + "</a>\n";
},"7":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "        <span class=\"task-title\" href=\"javascript:void(0)\">"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.theme : depth0)) != null ? stack1.name : stack1), depth0))
    + "</span>\n";
},"9":function(depth0,helpers,partials,data) {
  return "                  <li role=\"presentation\"><a class=\"action-trigger\" data-action=\"delete\" role=\"menuitem\" href=\"javascript:void(0)\">删除视频</a></li>\n";
  },"11":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, buffer = "                  <li class=\"\" role=\"presentation\"><a class=\"download-task noscript\" role=\"menuitem\" tabindex=\"-1\" href=\"";
  stack1 = lambda(((stack1 = (depth0 != null ? depth0.render : depth0)) != null ? stack1.path : stack1), depth0);
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\" download>下载视频</a></li>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, buffer = "    <div class=\"task-info\">\n        <div class=\"task-cover\">\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.download : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(3, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "        </div>\n\n\n    </div>\n    <div class=\"task-head\">\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.share_url : depth0), {"name":"if","hash":{},"fn":this.program(5, data),"inverse":this.program(7, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "        <div class=\"btn-group pull-right\">\n            <a class=\"task-cog action-trigger\" data-action=\"edit\" href=\"javascript:void(0)\" title=\"编辑视频\"><i class=\"fa fa-edit\"></i></a>\n            <em class=\"divider vertical\"></em>\n            <span class=\"dropdown dropup\">\n              <a class=\"task-cog\" href=\"javascript:;\" data-toggle=\"dropdown\"><i class=\"fa fa-cog\"></i></a>\n              <ul class=\"dropdown-menu dropdown-menu-right\" role=\"menu\">\n                  <li role=\"presentation\" class=\"dropdown-header\">更多视频操作</li>\n                  <li role=\"presentation\"><a class=\"action-trigger\" data-action=\"preview-theme\"  role=\"menuitem\" tabindex=\"-1\" href=\"javascript:void(0);\">预览主题</a></li>\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.showEditDelete : depth0), {"name":"if","hash":{},"fn":this.program(9, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  buffer += "                  <li class=\"\" role=\"presentation\"><a class=\"action-trigger\" data-action=\"edit\" role=\"menuitem\" tabindex=\"-1\" href=\"javascript:void(0)\">编辑视频</a></li>\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.download : depth0), {"name":"if","hash":{},"fn":this.program(11, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "              </ul>\n            </span>\n\n        </div>\n    </div>\n";
},"useData":true};

this["JST"]["orders/video_preview"] = {"1":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "                        <a class=\"btn btn-success action-btn\" href=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.render : depth0)) != null ? stack1.path : stack1), depth0))
    + "\" download><i class=\"fa fa-download\"></i> 下载视频</a>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, lambda=this.lambda, buffer = "<div class=\"modal-dialog modal-lg\">\n    <div class=\"modal-content\">\n        <div class=\"modal-header\">\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\" id=\"close\">×</span><span class=\"sr-only\">关闭</span></button>\n            <h4 class=\"modal-title\" id=\"myLargeModalLabel\">查看视频</h4>\n        </div>\n        <div class=\"modal-body\">\n            <div class=\"row\">\n                <div class=\"preview col-md-9\">\n                    <video id=\"video"
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\"video-js vjs-default-skin vjs-big-play-centered\" controls autoplay preload=\"metadata\" width=\"640px\" height=\"360px\"\n                    poster=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.render : depth0)) != null ? stack1.thumbnail_img : stack1), depth0))
    + "\"\n                    data-setup=\"{}\">\n                      ";
  stack1 = ((helpers.generateSource || (depth0 && depth0.generateSource) || helperMissing).call(depth0, ((stack1 = (depth0 != null ? depth0.render : depth0)) != null ? stack1.path : stack1), {"name":"generateSource","hash":{},"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += "\n                      <!--<source src=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.render : depth0)) != null ? stack1.path : stack1), depth0))
    + "\" type='video/mp4' />-->\n                      <p class=\"vjs-no-js\">为了观看预览视频，请允许浏览器启用 Javascript 脚本，或者考虑升级浏览器<a href=\"http://videojs.com/html5-video-support/\" target=\"_blank\">支持 HTML5 视频播放</a></p>\n                    </video>\n                </div>\n                <div class=\"info col-md-3\">\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.is_finished : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "                </div>\n            </div>\n        </div>\n    </div>\n</div>\n";
},"useData":true};

this["JST"]["orders/videos_main"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div id=\"pad-wrapper\">\n    <div class=\"page__header\">\n      <div class=\"container\">\n        <h3 class=\"title\">\n          <span>我的视频</span>\n          <div class=\"pull-right\">\n              <a class=\"btn btn-complement btn-create-video\" href=\"javascript:;\">创建视频</a>\n          </div>\n        </h3>\n      </div>\n    </div>\n\n\n    <div class=\"list-container section container\">\n        <div class=\"video-list video-gallery\"></div>\n        <div class=\"pagination-container text-center\"></div>\n    </div>\n</div>\n\n";
},"useData":true};

this["JST"]["themes/query_selector"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div class=\"js-style-filter style-filter row\">\n  <div class=\"style-category-header\">选择主题</div>\n  <div class=\"col-md-8\">\n    <div class=\"queries__list-filter pull-left\">\n      <ul class=\"category\">\n        <li data-cat-value=\"\" class=\"selected\">全部</li>\n        <li data-cat-value=\"1\" class=\"\">婚礼请柬</li>\n        <li data-cat-value=\"2\" class=\"\">微电影</li>\n      </ul>\n    </div>\n\n    <div class=\"col-md-4 queries__dropdown-filter\">\n      <select class=\"selectpicker\" name=\"sort\">\n        <option value=\"hot\">按热度排序</option>\n        <option value=\"time\">按时间排序</option>\n      </select>\n    </div>\n\n  </div>\n\n  <div class=\"filter-block\">\n    <div class=\"col-md-3\">\n      <div class=\"input-addon-container\">\n        <input type=\"text\" class=\"search search-theme form-control\" placeholder=\"搜索主题\">\n        <span class=\"addon-icon show-when-focus\">\n          <a class=\"search-trigger\" href=\"javascript:void(0)\"> <i class=\"fa fa-search\"></i> </a>\n        </span>\n      </div>\n    </div>\n  </div>\n</div>\n\n";
  },"useData":true};

this["JST"]["themes/theme"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<!-- single theme -->\n<div class=\"img-box sequence-view-container\">\n    <div class=\"sequence-trigger\">\n        <a href=\"javascript:void(0)\" preview-url=\""
    + escapeExpression(((helper = (helper = helpers.previewUrl || (depth0 != null ? depth0.previewUrl : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"previewUrl","hash":{},"data":data}) : helper)))
    + "\"><img src=\""
    + escapeExpression(((helper = (helper = helpers.thumbnail_img || (depth0 != null ? depth0.thumbnail_img : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"thumbnail_img","hash":{},"data":data}) : helper)))
    + "\" class=\"img-responsive\"></a>\n    </div>\n    <div class=\"info\">\n        <a href=\"javascript:;\" class=\"title\" theme-id=\""
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n          "
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "\n          <i class=\"fa fa-play-circle-o\"></i>\n        </a>\n        <span class=\"pull-right hot\"><i class=\"fa fa-heart\"></i> "
    + escapeExpression(((helper = (helper = helpers.render_count || (depth0 != null ? depth0.render_count : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"render_count","hash":{},"data":data}) : helper)))
    + "</span>\n    </div>\n</div>\n";
},"useData":true};

this["JST"]["themes/theme_in_order"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, lambda=this.lambda;
  return "<!-- theme in create order -->\n<td class=\"theme-info\">\n    <!--<input type=\"checkbox\">-->\n    <a class=\"preview pull-left\" href=\"javascript:void(0)\"><img src=\""
    + escapeExpression(((helper = (helper = helpers.thumbnail_img || (depth0 != null ? depth0.thumbnail_img : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"thumbnail_img","hash":{},"data":data}) : helper)))
    + "\" height=\"80px;\"></a>\n</td>\n<td>\n    <a class=\"name preview\" href=\"javascript:void(0)\" theme-id=\""
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "</a>\n</td>\n<td class=\"\">\n    "
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.category : depth0)) != null ? stack1.name : stack1), depth0))
    + "\n</td>\n<td>\n    <a class=\"delete-theme btn btn-round btn-round-mini btn-slight-danger btn-sleeping\" href=\"javascript:;\">\n      <i class=\"fa fa-remove\"></i>\n    </a>\n</td>\n\n";
},"useData":true};

this["JST"]["themes/theme_main"] = {"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  return "<div id=\"pad-wrapper\" class=\"theme-content gallery container\">\n    <div class=\"queries-container fluid-container\">\n      <!--for query filters-->\n    </div>\n    <div class=\"gallery-wrapper\">\n        <div class=\"row gallery-row theme-list\">\n          <!--for theme items -->\n        </div>\n        <div class=\"pagination-container text-center\">\n          <!--for paginator -->\n        </div>\n    </div>\n</div>\n\n";
  },"useData":true};

this["JST"]["themes/theme_preview"] = {"1":function(depth0,helpers,partials,data) {
  return "          <a class=\"btn btn-theme action-btn add-cart\"><i class=\"fa fa-shopping-cart\"></i> 加入购物车</a>\n          <a class=\"btn btn-complement action-btn create-order\"><i class=\"fa fa-plus\"></i> 创建订单</a>\n";
  },"3":function(depth0,helpers,partials,data) {
  return "          <a class=\"btn btn-complement action-btn create-order full-width\"><i class=\"fa fa-plus\"></i> 创建视频</a>\n";
  },"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, lambda=this.lambda, buffer = "<div class=\"modal-dialog modal-lg\">\n  <div class=\"modal-content\">\n    <div class=\"modal-header\">\n      <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\" id=\"close\">×</span><span class=\"sr-only\">关闭</span></button>\n      <h4 class=\"modal-title\" id=\"myLargeModalLabel\">预览主题</h4>\n    </div>\n    <div class=\"modal-body\">\n      <div class=\"theme-info row\">\n        <div class=\"preview col-md-9\">\n          <video id=\"video"
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\"video-js vjs-default-skin vjs-big-play-centered\" controls autoplay preload=\"metadata\" width=\"640px\" height=\"360px\"\n            poster=\""
    + escapeExpression(((helper = (helper = helpers.thumbnail_img || (depth0 != null ? depth0.thumbnail_img : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"thumbnail_img","hash":{},"data":data}) : helper)))
    + "\"\n            data-setup=\"{}\">\n            ";
  stack1 = ((helpers.generateSource || (depth0 && depth0.generateSource) || helperMissing).call(depth0, (depth0 != null ? depth0.previewUrl : depth0), {"name":"generateSource","hash":{},"data":data}));
  if (stack1 != null) { buffer += stack1; }
  buffer += "\n            <p class=\"vjs-no-js\">为了观看预览视频，请允许浏览器启用 Javascript 脚本，或者考虑升级浏览器<a href=\"http://videojs.com/html5-video-support/\" target=\"_blank\">支持 HTML5 视频播放</a></p>\n          </video>\n        </div>\n        <div class=\"info col-md-3\">\n          <h3 theme-id=\""
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "\">"
    + escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"name","hash":{},"data":data}) : helper)))
    + "</h3>\n          <p class=\"desc\">"
    + escapeExpression(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"description","hash":{},"data":data}) : helper)))
    + "</p>\n          <p class=\"id\">编号：<span>"
    + escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"id","hash":{},"data":data}) : helper)))
    + "</span></p>\n          <p class=\"author\">作者：<span>"
    + escapeExpression(((helper = (helper = helpers.author || (depth0 != null ? depth0.author : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"author","hash":{},"data":data}) : helper)))
    + "</span></p>\n          <p class=\"category\">类型：<span>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.category : depth0)) != null ? stack1.name : stack1), depth0))
    + "</span></p>\n          <p class=\"count\">热度：<span>"
    + escapeExpression(((helper = (helper = helpers.render_count || (depth0 != null ? depth0.render_count : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"render_count","hash":{},"data":data}) : helper)))
    + "</span></p>\n";
  stack1 = helpers['if'].call(depth0, (depth0 != null ? depth0.isVendor : depth0), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.program(3, data),"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "        </div>\n      </div>\n    </div>\n  </div>\n</div>\n";
},"useData":true};

return this["JST"];

});